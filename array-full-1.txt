----
Absolute slice() substring() fill(). They all ignore last index.
so length of result = difference between what you asked for.

----
Relative splice() substr()


----
slice() is absolute
slice ignores last index. Even though you explicitly asked for it (!).
[0,1,2,3,4,5].slice(1,2) // [1] Note: does not return index 2
[0,1,2,3,4,5].slice(0,arr.length - 1) // [0, 1, 2, 3, 4]. Last index is NOT returned.

----
fill() is absolute
fill modifies the array.
[0,1,2,3,4].fill(9,2,4) // [0, 1, 9, 9, 4] Does not fill index 4

----
substring() is absolute and substr() is relative
"Mozilla".substr(1,2) // "oz"
"Mozilla".substring(1,2) // "o"

----
all subarrays with 0 sum
longest subarray with equal 0s and 1s.
longest subarray with given sum
subarray with given sum. input will have only one that matches.
..
Use running sum. At each element, calculate sum of all elements seen so far. store this sum in a hashmap. if sum at element 19 and element 7 are same, that means, sum of everything between 7-19 must be 0.
replace 0 with -1 and find longest subarray with 0 sum

----
duplicate element in limited range array
(1) hashmap of visited elements (2) without hashmap, get value, go to index where index = value, make it negative. After 1 traversal all except duplicate will be negative.
----
find index of 0 to be replaced to get longest sequence of 1s
(Trick is when to reset the count) count 1's between last 0 and current 0. note when we were at previous 0, we did not set count to 0, instead we set the count to number of 1s before that 0.
----
longest sequence of 1's that can be made with at most k zeros
----
longest sequence of 1's and 0's. At most k zeros
----
longest sequence of n 1's and k 0's.
Idea is to use flexible sliding window. Typically the window size is fixed. But not here. Here we add elements from right. Till we have upto k 0s. Once that increases window is unstable. Then we remove elements from left till window is stable again. Record maximum window size.
----
smallest subarray with sum > k (Find length of smallest subarray whose sum of elements is greater than the given number)
use flexible sliding window. sum < k is unstable, sum > k is stable.


----
longest subarray formed by consecutive integers in any order.
O(n^3) consecutive subarray means (max - min = length of array)
----
Boyer-Moore majority vote algorithm
Finds majority element in linear time and constant space. Alternative is count frequency.
----
maximum product of 2 numbers in array
we compare product of smallest 2 and largest 2, and pick the bigger of these.
----
pair with given sum (two sum)
Store each num in hash. Pair found if target - current is in hash.
----
pair with given difference in CONSTANT SPACE i.e. no hashmap
--- triplet with given sum (three sum)
O(n^2) (1) recursion similar to 0-1 knapsack (2) add array to hashmap. then for each pair check if target - pair sum is in map.

sort array and use binary search to find element with given difference
----
subarray with max sum (kadane)
max_ending_here = max_ending_here + a[i]
max_ending_here = max(max_ending_here, 0);
max_so_far = max(max_ending_here, max_so_far)
----
print subarray with max sum (Print continuous subarray with maximum sum) kadane only finds max sum. does not give the max array. Use 3 extra variables ... 2 to track start, end of max sofar and one more to mark where sum starts getting positive.

----
circular subarray maximim sum (invert sign, then kadane)
invert sign then use kadane to find max negative contiguous array i.e. smallest sum array. Then everything else has to be array with max sum (after inverting sign)

----
merge 2 arrays X and Y. Both arrays are sorted. And there are Y.size() 0s in X. Idea is to move all 0x in X to the start. Then starting BACKWARDS, merge them both.

----
Rearrange an array such that evey alternate element is greater than its left and right elements.
Sort the array, then place one element from each end and place in new array.

----
Fisher-Yates shuffle
For each element, pick a random element from remaining array and swap. This is in place shuffle. Linear.

----
Dutch national flag / 3 way partition
pivot. lo/mid/high. NOTE: we pick pivot "VALUE" and compare each element "VALUE" to pivot VALUE. We are not picking a random pivot "ELEMENT" from the array but the pivot "VALUE".
-- sort array of 3 distinct elements in linear time and constant space

--?-- Decode array constructed from another array
Took too long to even understand the question. Skipped.




----
largest number possible from a set of (multi-digit) numbers
use custom comparator. direct sorting will not work. e.g. {13,100}  largest no is 13100. Standard sorting will give 10031. So sort numbers based on largest first digit of the number.

----
longest bitonic subarray. Bitonic = increasing then decreasing
Left to right track longest increasing sequence. Right to left track longest decreasing sequence starting at each index. Longest sequence is max of sum of these numbers.

----
Find maximum sum path involving elements of given arrays
needs a lot of variables. very error prone at slicing the indexes. keep track of subarrays between common elements in both arrays. and pick the largest sum one. make a final array.

----
subarray with maximum product
code looks so similar to kadane.

----
subarray of size k with minimum sum (sliding window)

----
longest common subarray in 2 arrays. Start, end index must be same in both arrays. ("Length of longest continuous sequence with same sum in given binary arrays")

Given two binary arrays X and Y, find the length of longest continuous sequence that starts and ends at same index in both arrays and have same sum
[0,0,0,0,0,1,1,1,1,1]. [1,1,1,0,0,1,1,1,1,0] answer -> [x,x,x,0,0,1,1,1,1,x]


----
4 sum problem
(1) Recursion with apporach of max(inclusion, exclusion). (2) Start calculating all pairs, and save (sum => pair) in hashmap. For each pair, if we see target - current sum in map, then check if any of the 4 elements are repeated. If not, we have found a quadruplet (2 pairs) that add to the sum,
----
All Distinct combinations of length k
similar to knapsack 0-1. Recursive solution
----
Longest increasing subsequence. Longest decreasing subsequence.
(1) recursion similar to knapsack. include/exclude pattern. (2) DP
----
Find maximum sum of subsequence with no adjacent element
Similar to knapsack 0-1. Recursive. include-exclude.


----
buy sell shares once
----
buy sell shares any number of times but stock only 1
----
buy sell shares any number of times and stock upto k

----
Find maximum sum of subsequence with no adjacent elements
Knapsack 0-1. Recursive.

----
Find minimum number of platforms needed in the station so to avoid any delay in arrival of any train
Count overlapping intervals. Sort both arrays. Similar to merging them compare top element, arrival > departure, increment, else decrement. If arrival==departure, pick depart i.e. depart train first them pick arrival.

----
Move all zeros present in an array to the end
-- using Extra space: Easiest is loop over, if non zero, add to array. When done, fill remaining places with 0.
-- using same array: Track next available zero, and replace with non zero number.

----
Rearrange array such that A[A[i]] is set to i for every element A[i]
we basically use the same
Use the fact that all values are between 0 - n-1. Use multiplication. 

----
Replace each element of an array with product of every other element without using division operator If we knew product of elements to left and elements to right, we could solve this. So just  use extra space, and find product of all elements to left in one aux array and to the right in another. Done in 2 separate passes. And in third pass, find the product for each element using these 2.