
# Javascript Advanced
<hr/>
### Shashank Pai

--

## JavaScript

* Released 1996 by Brendan Eich working on Netscape
* ECMA - European Computer Manufacturers Association. ECMAScript is the specification for JavaScript
* ES8 (2017). Latest. Not all features supported across browsers.
* ES6 (2015). Most features supported across most browsers IE11+

--

## Javascript Basics

* Object based - Everything is an object (Almost)
* Functional behaviour - Functions are first class citizens
* 5 Primitives - Number, String, Boolean, Null, Undefined
* Case Sensitive
* Loosely Typed - Types determined at runtime

~~

### Loose Typing

<pre>
<code class="hljs">
var a = 1;
var b = 1.2;
var result = a + b; //2.2
</code>
</pre>

<pre>
<code class="hljs">
 7  + 7 + "7"; // 147
"7" + 7 +  7 ; // 777
</code>
</pre>


--


## Creating Objects

* Literal Notation
* Using constructor and the "new" operator
* Object.create()

~~

### Literal notation
<pre>
  <code class="hljs">
    var john = {
      name : 'John',
      age : 21
    }
  </code>
</pre>

~~

### Constructor + "new" operator
<pre>
<code class="hljs" >
function Person(name, age){
  this.name = name;
  this.age = age;
}

var john = new Person("John", 21);

</code>
  </pre>

~~


### Object.create()
<pre>
  <code class="hljs">
    var john = Object.create(null);
    john.name = "John";
    john.age = "21";
  </code>
</pre>

--


## Objects are like hashtables
* Each property is a key-value pair
* Keys are unique
* Value can be any valid primitive, object or function

~~

### Object properties

* Dot and square bracket notation
<pre>
<code class="hljs" data-noescape data-trim>
  john.age; //21
john['age']; //21
</code>
</pre>

* Bracket notation allows dynamic access to values

<pre>
<code class="hljs" data-noescape data-trim>
var john = {name : "john", age: 21, hair: "black"}

var key1 = "name"; // keys are strings
var key2 = "age";  // not hardcoded variables
var key3 = "hair"; // string values can be changed dynamically

console.log(john[key1]); //john
console.log(john[key2]); //21
console.log(john[key3]); //black

</code>
</pre>


~~

* Looping over object properties

<pre>
<code class="hljs" data-noescape data-trim>
var john = {name : "john", age: 21, hair: "black"}

for (prop in john){
	console.log("%s - %s", prop, john[prop]);
}
</code>
<code>name - john
age - 21
hair - black
</code>
</pre>



--
### Functions in JS

* Functions are also objects. Except they are executable.
* Can assign them to variables ( Closure )
* Can pass them around as parameters ( Closure )
* Can return them from other functions ( Closure )
* Can define functions within functions
* Can call functions using apply/call
* Functions are "Hoisted"
* Parameters are optional



~~
#### 2 ways to declare functions


<pre>
<code class="hljs" data-noescape data-trim>
function foo() {
  ...
}
</pre>
</code>

<!--  -->

<pre>
<code class="hljs">
// function assigned to variable "foo"
var foo = function() {
  ...
}
</pre>
</code>

~~

#### Pass functions as arguments

<pre>
<code class="hljs" data-noescape data-trim>
function french(){ return "Bonjour"; }
function spanish(){ return "Hola" }

function sayHelloIn(lang){
  var msg = lang();
  console.log(msg);
}

sayHelloIn(french); // Bonjour;
sayHelloIn(spanish); // Hola
</pre>
</code>

~~

#### Return functions instead of values

<pre>
<code class="hljs" data-noescape data-trim>
function getLang(langName){
  var  frenchFn = function french() { return "Bonjour"; }
  var spanishFn = function spanish(){ return "Hola" }

  if (langName == "french") { <mark>return</mark> frenchFn }
  if (langName == "spanish"){ <mark>return</mark> spanishFn }

}

function sayHelloIn(lang){
  var langFn = getLang(lang); // returns a function
  var msg = langFn();         // calls the function
  console.log(msg);
}

</pre>
</code>


<pre>
<code class="hljs" data-noescape data-trim>
sayHelloIn("french") // Bonjour;
sayHelloIn("spanish"); // Hola
</pre>
</code>

~~

### Inner function example

<pre>
<code class="hljs" data-noescape>
function Product(inputPrice){
  var price = inputPrice;
  var taxRate = 8.5;

  var calcPriceWithTax = <mark>function</mark>(){
    return price + (price * taxRate/100);
  }
}
</pre>
</code>

~~

### Calling functions

<pre>
<code class="hljs" data-noescape data-trim>
function concat(a,b){
  console.log(a+b);
}
</code>

Usual way
<code>
concat("Hello", " world");
</code>

Using "call"
<code>
concat.call(null, "Hello", " world");
</code>

Using "apply"
<code>
concat.apply(null, ["Hello", " world"]);
</code>
</pre>
~~

### Optional arguments

<pre>
<code class="hljs" data-noescape data-trim>
function add(a,b,c,d){
    var total = 0;
    if (a){total +=a}
    if (b){total +=b}
    if (c){total +=c}
    if (d){total +=d}
    return total;
}

add(1);       // 1
add(1,2);     // 3
add(1,2,3);   // 6
add(1,2,3,4); // 10
</pre>
</code>

~~

#### Hoisting

<pre>
<code class="hljs">
sayHi();
// Works because sayHi definition below
// was "Hoisted" before this was executed
...
...
function sayHi() {
  console.log("Hi !");
}
</code>
<pre>

--

### Inner functions
* vars and methods are private - not accessible externally.
* Inner functions can access parent/outer function's private vars
* Privileged functions allow external access without allowing modification

~~

### Inner Function example
* Vars are visible to inner functions
* But are not accessible from outside

<pre>
<code class="hljs" data-noescape>
function Product(inputPrice){
  <mark>var</mark> price = inputPrice; // private
  <mark>var</mark> taxRate = 8.5;      // private

  <mark>var</mark> calcPriceWithTax = function(){ // private
    return price + (price * taxRate/100);
  }
}

var shampoo = new Product(20);

console.log(shampoo.price); // undefined
shampoo.calcPriceWithTax(); // Error
</pre>
</code>

~~

### External access using "this"
* Properties/methods can be exposed by attaching to the object using "this"
* But that also allows modification

<pre>
<code class="hljs" data-noescape>
function Product(inputPrice){
  <mark>this</mark>.price = inputPrice; // price is now public
}

var shampoo = new Product(20);
console.log(shampoo.price); // 20
shampoo.price = 10;
console.log(shampoo.price); // 10
</pre>
</code>

~~

### External access via "privileged" function
<pre>
<code class="hljs" data-noescape data-trim>
function Product(inputPrice){
  var price = inputPrice;
  var taxRate = 8.5;

  var calcPriceWithTax = function(){
    return price + (price * taxRate/100);
  }

  // create a privlieged function to expose "calcPriceWithTax"
  <mark>this</mark>.finalPrice = function(){
    return calcPriceWithTax();
  }
}

var shampoo = new Product(20);
shampoo.finalPrice() // 21.7
</pre>
</code>


--

### Prototype is javascript way of inheritance.

<table>
<thead> <tr><th>Java</th> <th> Javascript</th></tr></thead>
<tbody>
<tr><td>classical</td> <td> prototypal</td></tr>
<tr><td>classes</td> <td>functions</td></tr>
<tr><td>methods</td> <td>functions</td></tr>
<tr><td>constuctors</td> <td>functions</td></tr>
</tbody>
</table>

~~

### Prototype Chain
How does js lookup value for <code> obj.key </code>?
* Every object has an internal property called "[[Prototype]]".
* If a property is not found in obj, runtime looks in the "[[Prototype]]"
* If that fails, lookup in "[[Prototype]]" of "[[Prototype]]".
* Search continues upwards till root "Object" is reached.


~~

### Simple prototype chain

<em>Function</em>
<pre>
<code class="hljs" data-noescape data-trim>
 function Person() {}
</pre>
</code>

<em>Prototype chain</em>
<pre>Person  -> Function.prototype -> Object.prototype -> (end)</pre>

---------------

<em>Object</em>
<pre>
<code class="hljs" data-noescape data-trim>
var p = new Person();
</pre>
</code>

<em>Prototype chain</em>
<pre>p -> Person.prototype  -> Function.prototype -> Object.prototype -> (end)</pre>

<!-- source : https://stackoverflow.com/questions/572897/how-does-javascript-prototype-work -->

~~
### Simple Prototype Chain
![alt text](img/Prototype-chain-1.png "Prototype Chain")

~~

### Inheritance prototype chain

<pre>
<code class="hljs" data-noescape data-trim>
function Thief() { }
var p = new Person();

Thief.prototype = p; // explicitly set the prototype
var t = new Thief();
</code>
</pre>

<em>Prototype chain</em>

<pre>t -> p -> Person.prototype -> Object.prototype (end)</pre>


~~
### Inheritance Prototype Chain
![alt text](img/Prototype-chain-2.png "Prototype Chain")


<!--  <iframe width="100%" height="500" src="//jsfiddle.net/j5U8P/4/embedded/js,result/dark/" allowfullscreen="allowfullscreen" frameborder="0"></iframe> -->


--

### Syntax and common functions
* Arrays
** forEach() **
** map() **
* "=>" notation
* (function (){})() to call immediately after declaration
* JSON.parse and JSON.stringify
* for (key in obj){} notation

~~

### Array.forEach()
* Convenience function to loop over array
* Does not return


<pre>
<code data-noescape data-trim>
  ["Sun" , "Mon", "Tue"]
    .forEach(function(day){
      console.log("The day is %s", day) })
</code>
</pre>

<pre>
<code class="hljs" data-noescape data-trim>
The day is Sun
The day is Mon
The day is Tue
</code>
</pre>

~~

### Array.map()
* Convenience method for transforming/mapping an array
* Returns mapped array

<pre id="wrapper">
<code id = "fl-left" class="hljs" data-noescape data-trim>
["Sun" , "Mon", "Tue"]
  .map( function(day){
      return day.toUpperCase()
    });
</code>

<pre>
<code class="hljs" data-noescape data-trim>
["SUN", "MON", "TUE"]
</code>
</pre>

~~

### "=>" (arrow) notation

<pre>
<code class="hljs" data-noescape data-trim>
var addOne = function(i){ return i+1; }
</code>
<em> is same as </em>
<code>
var addOne = (i) => {return i+1;}
</code>
<em> is same as </em>
<code>
var addOne = i => {return i+1;}
</code>
<em> is same as </em>
<code>
var addOne = i => i+1;
</code>
</pre>

~~

### Array.forEach() and .map() in arrow notation

<pre>
<code data-noescape data-trim>
  ["Sun" , "Mon", "Tue"]
  .forEach(day => {console.log("The day is %s", day)})
</code>

<code class="hljs" data-noescape data-trim>
["Sun" , "Mon", "Tue"]
.map(day => day.toUpperCase());
</code>
</pre>

~~

### IIFE (Immediately Invoked Function Expression)
<pre>
<code class="hljs" data-noescape data-trim>
var x = (function addOne(i) {
  return i + 1;
  })(5);
//function is immediately called with argument "5"
console.log(x); // 6;
</code>
</pre>

Same function in arrow notation
<pre>
<code class="hljs" data-noescape data-trim>
var x = (i => i + 1)(5);
console.log(x); // 6;
</code>
</pre>

~~

## Recursion
<pre>
<code class="hljs" data-noescape data-trim>
function fact(n){
  if(n ==1){
    return 1;
  }
  return n*fact(n-1)
}
</code>
<em>is same as </em>

<code class="hljs" data-noescape data-trim>
fact = n => n == 1?1:n*fact(n-1)
</code>
<code>
fact(5); //120;
</code>
</pre>

<!--
<pre>
<code class="hljs">
</code>
</pre>
-->
